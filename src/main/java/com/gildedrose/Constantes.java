package com.gildedrose;

public class Constantes {

    public Constantes(){}

    final String AGED_BRIE = "Aged Brie";
    final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    final String BACKSTAGES_PASSES = "Backstage passes to a TAFKAL80ETC concert";
    final String CONJURED = "Conjured Mana Cake";
}
