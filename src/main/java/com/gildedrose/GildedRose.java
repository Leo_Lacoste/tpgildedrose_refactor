package com.gildedrose;

import com.gildedrose.Items.Item;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] items) {
        this.items = items;
    }

    public void updateQuality() {
        for (Item i : items) {
            i.updateQuality();
            System.out.println(i);
        }
    }
}