package com.gildedrose.Items;

public class Conjured extends Item{
    public Conjured(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        if(quality > 0) {
            if (sellIn < 0) {
                quality -= 4;
            } else {
                quality -= 2;
            }
        }
        sellIn -= 1;
    }
}
