package com.gildedrose.Items;

public class BackstagePasses extends Item{
    public BackstagePasses(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        if(quality<50){
            if(sellIn <= 0){
                quality = 0;
            }else if(sellIn <= 5){
                quality += 3;
            }else if(sellIn <= 10){
                quality += 2;
            }else{
                quality += 1;
            }
        }
        sellIn -= 1;
    }
}
