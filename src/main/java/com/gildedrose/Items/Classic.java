package com.gildedrose.Items;

public class Classic extends Item{
    public Classic(String name, int sellIn, int quality) {
        super(name, sellIn, quality);
    }

    @Override
    public void updateQuality() {
        if(quality > 0) {
            if (sellIn < 0) {
                quality -= 2;
            } else {
                quality -= 1;
            }
        }
        sellIn -= 1;
    }
}
