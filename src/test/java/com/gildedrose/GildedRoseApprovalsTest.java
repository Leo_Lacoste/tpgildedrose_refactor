package com.gildedrose;

import com.gildedrose.Items.*;
import org.approvaltests.Approvals;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GildedRoseApprovalsTest {

    private Item[] items;
    GildedRose app;
    Constantes constantes = new Constantes();

    private static final int DAYS = 2;


    @Test
    void ApprovalTest() {
        items = new Item[] {
                new Classic("+5 Dexterity Vest", 10, 20), //
                new AgedBrie(constantes.AGED_BRIE, 2, 0), //
                new Classic("Elixir of the Mongoose", 5, 7), //
                new Sulfuras(constantes.SULFURAS, 0, 80), //
                new Sulfuras(constantes.SULFURAS, -1, 80),
                new BackstagePasses(constantes.BACKSTAGES_PASSES, 15, 20),
                new BackstagePasses(constantes.BACKSTAGES_PASSES, 10, 49),
                new BackstagePasses(constantes.BACKSTAGES_PASSES, 5, 49),
                new Conjured(constantes.CONJURED, 3, 6) };

        app = new GildedRose(items);

        for (int i = 0; i < DAYS; i++) {
            app.updateQuality();
        }

        Approvals.verifyAll("",items);
    }
}
