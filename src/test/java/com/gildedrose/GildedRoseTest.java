package com.gildedrose;

import com.gildedrose.Items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedRoseTest {

    private Item[] items;
    GildedRose app;
    Constantes constantes = new Constantes();

    @Test
    void ClassicObjects() {
        items = new Item[] { new Classic("foo", 0, 0)
                ,new Classic("pizza", 10, 5)
                ,new Classic("tarte",-1,7)};
        app = new GildedRose(items);

        app.updateQuality();
        assertEquals("foo", app.items[0].name);
        assertEquals(-1,app.items[0].sellIn);
        assertEquals(0,app.items[0].quality);

        assertEquals("pizza", app.items[1].name);
        assertEquals(9,app.items[1].sellIn);
        assertEquals(4,app.items[1].quality);

        assertEquals("tarte", app.items[2].name);
        assertEquals(-2,app.items[2].sellIn);
        assertEquals(5,app.items[2].quality);
    }

    @Test
    void AgedBrieObjects() {
        items = new Item[] { new AgedBrie(constantes.AGED_BRIE, 5, 4)
                , new AgedBrie(constantes.AGED_BRIE, 5, 50)};
        app = new GildedRose(items);

        app.updateQuality();
        assertEquals("Aged Brie", app.items[0].name);
        assertEquals(4,app.items[0].sellIn);
        assertEquals(5,app.items[0].quality);
        assertEquals("Aged Brie", app.items[1].name);
        assertEquals(4,app.items[1].sellIn);
        assertEquals(50,app.items[1].quality);
    }

    @Test
    void SulfurasObject() {
        Item[] items = new Item[] { new Sulfuras(constantes.SULFURAS, 0, 80) };
        app = new GildedRose(items);

        app.updateQuality();
        assertEquals("Sulfuras, Hand of Ragnaros", app.items[0].name);
        assertEquals(0,app.items[0].sellIn);
        assertEquals(80,app.items[0].quality);
    }

    @Test
    void BackStageObjects() {
        Item[] items = new Item[] { new BackstagePasses(constantes.BACKSTAGES_PASSES, 14, 30)
                ,new BackstagePasses(constantes.BACKSTAGES_PASSES, 10, 45)
                ,new BackstagePasses(constantes.BACKSTAGES_PASSES, 5, 47)
                ,new BackstagePasses(constantes.BACKSTAGES_PASSES, 12, 50)
                ,new BackstagePasses(constantes.BACKSTAGES_PASSES, 0, 13) };
        app = new GildedRose(items);

        app.updateQuality();
        assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[0].name);
        assertEquals(13,app.items[0].sellIn);
        assertEquals(31,app.items[0].quality);

        assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[1].name);
        assertEquals(9,app.items[1].sellIn);
        assertEquals(47,app.items[1].quality);

        assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[2].name);
        assertEquals(4,app.items[2].sellIn);
        assertEquals(50,app.items[2].quality);

        assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[3].name);
        assertEquals(11,app.items[3].sellIn);
        assertEquals(50,app.items[3].quality);

        assertEquals("Backstage passes to a TAFKAL80ETC concert", app.items[4].name);
        assertEquals(-1,app.items[4].sellIn);
        assertEquals(0,app.items[4].quality);
    }

    @Test
    void ConjuredObjects() {
        items = new Item[] { new Conjured(constantes.CONJURED, 0, 0)
                ,new Conjured(constantes.CONJURED, 10, 5)
                ,new Conjured(constantes.CONJURED, -1, 5)};

        app = new GildedRose(items);

        app.updateQuality();
        assertEquals("Conjured Mana Cake", app.items[0].name);
        assertEquals(-1,app.items[0].sellIn);
        assertEquals(0,app.items[0].quality);

        assertEquals("Conjured Mana Cake", app.items[1].name);
        assertEquals(9,app.items[1].sellIn);
        assertEquals(3,app.items[1].quality);

        assertEquals("Conjured Mana Cake", app.items[2].name);
        assertEquals(-2,app.items[2].sellIn);
        assertEquals(1,app.items[2].quality);
    }

}
