package com.gildedrose;

import com.gildedrose.Items.*;

public class TexttestFixture {
    public static void main(String[] args) {

        Constantes constantes = new Constantes();

        Item[] items = new Item[] {
                new Classic("+5 Dexterity Vest", 10, 20), //
                new AgedBrie(constantes.AGED_BRIE, 2, 0), //
                new Classic("Elixir of the Mongoose", 5, 7), //
                new Sulfuras(constantes.SULFURAS, 0, 80), //
                new Sulfuras(constantes.SULFURAS, -1, 80),
                new BackstagePasses(constantes.BACKSTAGES_PASSES, 15, 20),
                new BackstagePasses(constantes.BACKSTAGES_PASSES, 10, 49),
                new BackstagePasses(constantes.BACKSTAGES_PASSES, 5, 49),
                new Conjured(constantes.CONJURED, 3, 6) };

        GildedRose app = new GildedRose(items);

        int days = 2;
        if (args.length > 0) {
            days = Integer.parseInt(args[0]) + 1;
        }

        for (int i = 0; i < days; i++) {
            System.out.println("-------- day " + (i+1)+ " --------");
            System.out.println("name, sellIn, quality");
            app.updateQuality();
            System.out.println();
        }
    }

}
