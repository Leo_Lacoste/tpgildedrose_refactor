# TPGildedRose_Refactor


## Getting started

Presentation de notre version de refactor du projet Gilded Rose

## Get the project

```
git clone https://gitlab.com/Leo_Lacoste/tpgildedrose_refactor.git
```

## Name
Refactor Kata du projet Gilded Rose (Java version)

## Description
Refactor du projet Gilded Rose en respectant le plus possible de bonnes pratiques de code (principes SOLID, approvals tests, TDD, etc)

## Roadmap
- Créer un filet de tests unitaires pour vérifier le respect des règles
- Refactorer le code existant en effectuant un nombre de minimum de changements pour simplement rendre le code plus lisible sans modifier la structure du projet (suppression de conditions inutiles, réunion de conditions possibles, changements mineurs d’instructions évidentes)
- Modifier la structure du projet en respectant le principe SRP (Single Responsibility Principle) en créant pour chaque objet qui a un comportement unique, une classe
- Modifier la classe « Item » pour qu’elle devienne une classe mère de tous les autres objets que l’on va créer. Une méthode « updateQuality » que tous les objets (enfants) qui vont hériter de « Item » vont implémenter pour gérer eux-même leur comportement
- Création d’une classe ClassicObjet qui gérera elle-même le comportement (mise à jour de la qualité) des objets classiques
- Modifier les tests en conséquences pour prendre en compte cette nouvelle classe
- Faire pareil que les deux dernières améliorations pour un objet  « Aged Brie »
- Faire pareil que les deux dernières améliorations pour un objet  « Backstage Passes »
- Faire pareil que les deux dernières améliorations pour un objet  « Sulfuras »
- Adaptation du scénario de test pour prendre en compte les nouvelles classes
- Création des tests pour l’objet « Conjured » en se basant sur les spécificités données
- Création de l’objet « Conjured » en se basant sur les autres objets
- Adaptation du scénario de test pour prendre le nouvel objet Conjured
- Génération d’un filet de tests « approvals » sur le scénario de test (dans TextTestFixture) en utilisant un fichier pour sauvegarder le résultat attendu

## Contributing
Projet issu de la base du "Gilded Rose" Java (cf : https://github.com/ChrisHeral/GildedRose-Refactoring-Kata)

## Authors and acknowledgment
Arnault Douyere & Léo Lacoste

## Project status
Refactor fini

